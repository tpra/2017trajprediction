import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import *
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
import pickle
from sklearn.externals import joblib

def oheDecode(data):
    data2 = np.zeros(data.shape[0])
    for i in range(data.shape[0]):
        tmp = np.where((data[i,:]) == np.max(data[i,:]))
        try:
            data2[i] = tmp[0][0]
        except:
            print(data[i,:], tmp[0])
            quit()
    return data2

retrain = True

lf = '5s'

with open('nnsets_us101_lf' + lf + '.pkl', 'rb') as f:
    (trainX, trainY, testX, testY, _, _, _, _, features) = pickle.load(f)


# remove = np.where(np.logical_and(trainY[:,0] == 1, np.random.rand(trainY.shape[0]) > 0.5))
# print("Thinning class 0: removing", len(remove[0]), "elements")
# trainX = np.delete(trainX, remove, 0)
# trainY = np.delete(trainY, remove, 0)

testY = oheDecode(testY)

trainCfMat = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
testCfMat = confusion_matrix(testY, testY)
cw = {i: sum(trainCfMat[j,j] for j in range(trainCfMat.shape[0]))/trainCfMat.shape[0]/trainCfMat[i,i] for i in range(trainCfMat.shape[0])}

bootstrap = False
undersample = True

trainCfMatBS = trainCfMat

if bootstrap:
    print("Bootstrapping")
    maxCatNum = np.max(np.diag(trainCfMat))
    maxCatId = np.argmax(np.diag(trainCfMat))

    trainXBS = np.zeros((0, trainX.shape[1]))
    trainYBS = np.zeros((0, trainY.shape[1]))

    # Bootstrap train set to equalize classes
    for i in range(trainY.shape[1]):
        if i == maxCatId:
            continue
        catIdx = np.where(trainY[:,i] == 1)
        bsIdx = np.random.choice(catIdx[0], max(0,maxCatNum-catIdx[0].shape[0]))
        extract = trainX[bsIdx, :]
        noise = extract*0.1*np.random.normal(0,0.1,extract.shape)
        trainXBS = np.concatenate((trainXBS, extract+noise), axis=0)
        trainYBS = np.concatenate((trainYBS, trainY[bsIdx, :]), axis=0)

    trainX = np.concatenate((trainX, trainXBS), axis=0)
    trainY = np.concatenate((trainY, trainYBS), axis=0)
    print("finished bootstrapping")

    del trainXBS
    del trainYBS

    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat)/sum(np.diag(trainCfMat)))
    print("training set with bootstrap:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))
    print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat)/sum(np.diag(testCfMat)))
    print("classes weights:", cw)
if undersample:
    print("Undersampling")
    minCatNum = np.min(np.diag(trainCfMat))
    maxCatId = np.argmax(np.diag(trainCfMat))

    trainXBS = np.zeros((0, trainX.shape[1]))
    trainYBS = np.zeros((0, trainY.shape[1]))

    # Bootstrap train set to equalize classes
    for i in range(trainY.shape[1]):
        catIdx = np.where(trainY[:,i] == 1)
        np.random.shuffle(catIdx)
        bsIdx = catIdx[0][:minCatNum]
        extract = trainX[bsIdx, :]
        trainXBS = np.concatenate((trainXBS, extract), axis=0)
        trainYBS = np.concatenate((trainYBS, trainY[bsIdx, :]), axis=0)

    trainX = trainXBS
    trainY = trainYBS
    print("finished undersampling")

    del trainXBS
    del trainYBS

    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat)/sum(np.diag(trainCfMat)))
    print("training set after undersampling:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))
    print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat)/sum(np.diag(testCfMat)))
    print("classes weights:", cw)


trainCfMat = trainCfMatBS
cw = {i: sum(trainCfMat[j,j] for j in range(trainCfMat.shape[0]))/trainCfMat.shape[0]/trainCfMat[i,i] for i in range(trainCfMat.shape[0])}

trainY = oheDecode(trainY)


reorder = (np.array([i for i in range(trainY.shape[0])]))
np.random.shuffle(reorder)
trainX = trainX[reorder,:]
trainY = trainY[reorder]

#clf = RandomForestClassifier(n_jobs=-1, n_estimators=100, random_state=0, max_depth=25)
clf = AdaBoostClassifier(n_estimators=250, random_state=0)
clf.fit(trainX, trainY, sample_weight=np.array([cw[i] for i in trainY]))

with open('adaboost_undersample_lf' + lf + '.pkl', 'wb') as f:
    pickle.dump(clf, f)

print("Score train:", clf.score(trainX, trainY), "test:", clf.score(testX,testY))

predTrain = clf.predict(trainX)
predTest = clf.predict(testX)

print("Confisuion matrices:")
print("Train\n",confusion_matrix(trainY, predTrain))
print("Test\n",confusion_matrix(testY, predTest))