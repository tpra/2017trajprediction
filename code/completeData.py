import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from time import time
import pickle

def date_parser(str):
    return pd.to_datetime(str, unit='ms')

def get8Surrounding(dataT, vid):
    # ret:
    # 0     unassigned
    # 1     rear left (follower of 4)
    # 2     rear (follower of 5)
    # 3     rear right (follower of 6)
    # 4     left (closest to the left)
    # 5     self
    # 6     right (closest to the right)
    # 7     front left (leader of 4)
    # 8     front (leader of 5)
    # 9     front right (leader of 6)
    ret = [0 for i in range(10)]
    ret[5] = vid
    selfpos = np.array(dataT[['locX', 'locY']].loc[vid])
    dist = np.sqrt(np.sum(np.square(np.array(dataT[['locX', 'locY']]) - selfpos),1))
    listV = np.unique(dataT.index.get_level_values('vid'))
    distM = {listV[i]: dist[i] for i in range(len(listV))}
    for i in range(len(listV)):
        v = listV[i]
        if v != vid:
            pos = dataT[['locX', 'locY']].loc[v]
            if pos['locX'] < selfpos[0]:
                if ret[4] == 0 or distM[ret[4]] > distM[v]:
                    ret[4] = v
            if pos['locX'] > selfpos[0]:
                if ret[6] == 0 or distM[ret[6]] > distM[v]:
                    ret[6] = v
    for i in [4,5,6]:
        if ret[i] > 0:
            ret[i+3] = dataT['leader'].loc[ret[i]]
            ret[i-3] = dataT['follower'].loc[ret[i]]
    return ret


def treatLane(lane):
    # [0]: +1 if leftmost lane, -1 if rightmost lane, 0 otherwise
    # [1]: +1 if aux lane, 0 otherwise
    # [2]: +1 if on-ramp, 0 otherwise
    # [3]: +1 if off-ramp, 0 otherwisec
    return np.array([(lane == 1).astype(int) - (lane == 6), lane == 7, lane == 8, lane == 9]).T

def findInList(listWhere, listStr):
    return [i for i in range(len(listWhere)) if listWhere[i] in listStr]


names = ['vid',             # id of the vehicle
         'frame',           # id of the frame
         'totframes',       # number of frames in which the vehicle appears
         'epoch',            # global time (ms)
         'locX',            # lateral coordinate of the front center of the vehicle (ft)
         'locY',            # longitudinal coordinate of the front center of the vehicle (ft)
         'globX',           # global X (ft)
         'globY',           # global Y (ft)
         'len',             # length of the vehicle (ft)
         'wid',             # width of the vehicle (ft)
         'class',           # class: 1 motorcycle, 2 auto, 3 truck
         'vel',             # instantaneous velocity (ft/s)
         'acc',             # instantaneous acceleration (ft/sq²)
         'lane',            # lane number (file dependent)
         'leader',          # id of the lead vehicle in the same lane, 0 if error
         'follower',        # id of the following vehicle in the same lane, 0 if error
         'spacing',         # distance between front-center of vehicle and front-center of the leader (ft)
         'headway'          # time from front-center of vehicle to front-center of the leader (s), 9999.99 if error
         ]

#file = "data/us101/trajectories-0820am-0835am_ok.txt_8s.pkl"
#suffix = "us101-0820"
filenames = {'us101': ["data/us101/trajectories-0750am-0805am_ok.txt_8s.pkl",
                   "data/us101/trajectories-0805am-0820am_ok.txt_8s.pkl",
                   "data/us101/trajectories-0820am-0835am_ok.txt_8s.pkl"]}
filenames = {             'i80': ["data/i80/trajectories-0400-0415_ok.txt_8s.pkl",
                     "data/i80/trajectories-0500-0515_ok.txt_8s.pkl",
                     "data/i80/trajectories-0515-0530_ok.txt_8s.pkl"]
             }
suffixes = {'us101': ["us101-0750", "us101-0805", "us101-0820"],
            'i80': ["i80-0400", "i80-0500", "i80-0515"]}


obsTime = 5
predTime = 5
scale = 1000
sampling = 100

for (suff, fnames) in filenames.items():
    suffX = {}
    col = None
    for i in range(len(fnames)):
        file = fnames[i]
        suffix = suffixes[suff][i]
        np.random.seed(0)
        with open(file, 'rb') as fn:
            data = pickle.load(fn)
        data_idx = data.set_index(['vid', 'epoch'])

        # list of features to be extracted from the csv (for all 8 surrounding vehicles, plus self)
        # local X, Y, length, width, vx and vy, acceleration and class
        ft = ['locX', 'locY', 'len', 'wid', 'vx', 'vy', 'acc', 'class', 'lane']
        # list of features to be extracted only for self
        # headway, id of leader, indicators for lane
        ftSelf = ['headway'] + ['s' + str(i) for i in [1,2,3,4,6,7,8,9]] + [ 'l0', 'l1', 'l2', 'l3']
        listft = ft + ftSelf + [f + '_s' + str(i) for f in ft for i in [1,2,3,4,6,7,8,9,88]] + ['vid_s88']
        # list of targets to be extracted
        listtarg = ['locX', 'locY']

        # append info on the leading vehicle
        datanew = data[['vid', 'epoch'] + ft + ftSelf]
        for i in [1,2,3,4,6,7,8,9]:
            datanew = pd.merge(datanew, data_idx[ft], how='left', left_on=['s' + str(i), 'epoch'], right_index=True, suffixes=('', '_s' + str(i)))
        datanew = pd.merge(datanew, data[data['s2'] > 0][ft+['s2','epoch','vid']], how='left', left_on=['s8', 'epoch'], right_on=['s2', 'epoch'], suffixes=('', '_s88'))

        # remove NAs
        datanew = datanew.fillna(value=0)
        data_idx = datanew.set_index(['vid', 'epoch'])

        #    print(datanew.head().columns)

        numfeat = 0
        numtarg = 0
        datT = data_idx[listft]
        indices = np.asarray(list(datT.index.values))
        listT = indices[:,1]
        datT = datT.values

        startT = min(listT)
        endT = max(listT)
        print("start", startT, "end", endT, "dur", (endT-startT)/1000)
        print("Computing dataset")

        X = {}
        listv = np.unique(indices[:,0])
        print(listv)
        tmp = datanew[['vid', 'epoch'] + listft]
        col = tmp.columns
        npdata = tmp.as_matrix()
        for v in listv:
            X[v] = npdata[npdata[:,0] == v, :]
        with open('dataset_8s_%s.pkl' % suffix, 'wb') as file:
            pickle.dump((X, col), file=file, protocol=pickle.HIGHEST_PROTOCOL)

        suffX = {**suffX, **X}

    with open('dataset_8s_%s-all.pkl' % suff, 'wb') as file:
        pickle.dump((suffX, col), file=file, protocol=pickle.HIGHEST_PROTOCOL)