from keras.models import Sequential, load_model, model_from_json
from keras.layers import *
from keras import backend as K
from keras import optimizers
import tensorflow as tf
from tensorflow.python.client import timeline
from keras import metrics
from keras.layers import Activation
from sklearn.preprocessing import *
from sklearn.metrics import *
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
# fix random seed for reproducibility
np.random.seed(0)
import pickle
from overundersample import *

x = np.linspace(-4*np.pi,4*np.pi,1000)
y = np.sin(x)
xtest = -4*np.pi + 8*np.pi*np.random.uniform(0,1,(1000))

ytest = np.sin(xtest)

model = Sequential()
model.add(Dense(10,activation='relu',input_dim=(1)))
model.add(Dense(10,activation='relu'))
model.add(Dense(10,activation='relu'))
model.add(Dense(10,activation='relu'))
model.add(Dense(1,activation='relu'))

model.compile('adam', 'mse')

model.fit(np.reshape(x, (x.shape[0], 1)), y, epochs=50)

yp = model.predict(np.reshape(x, (x.shape[0], 1)))
yt = model.predict(np.reshape(xtest, (xtest.shape[0], 1)))
print("Validating")
print(model.evaluate(np.reshape(x, (x.shape[0], 1)), y), model.evaluate(np.reshape(xtest, (xtest.shape[0], 1)), ytest))

plt.plot(x,y,'.')
plt.plot(x,yp, 'x')
plt.plot(xtest, ytest, 'o')
plt.plot(xtest, yt, '+')
plt.show()