import os
from keras.models import Sequential, load_model, model_from_json
from keras.layers import *
from keras import backend as K
from keras import optimizers
import tensorflow as tf
from tensorflow.python.client import timeline
from keras import metrics
from keras.layers import Activation
from sklearn.preprocessing import *
from sklearn.metrics import *
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
# fix random seed for reproducibility
np.random.seed(0)
from overundersample import *
import pickle

def oheDecode(data):
    data2 = np.zeros(data.shape[0])
    for i in range(data.shape[0]):
        tmp = np.where((data[i,:]) == np.max(data[i,:]))
        try:
            data2[i] = tmp[0][0]
        except:
            print(data[i,:], tmp[0])
            quit()
    return data2

retrain = True


lf = '5'

with open('nnsets_us101_lf' + lf + '.pkl', 'rb') as f:
    (_, trainY, _, testY, trainXLSTM, testXLSTM, _, _, _, _, features) = pickle.load(f)


# with open('nnsets_i80_lf' + lf + '.pkl', 'rb') as f:
#     (_, trainYi80, _, testYi80, trainXLSTMi80, testXLSTMi80, _, _, _, _, features) = pickle.load(f)

# remove = np.where(np.logical_and(trainY[:,0] == 1, np.random.rand(trainY.shape[0]) > 0.5))
# print("Thinning class 0: removing", len(remove[0]), "elements")
# trainXLSTM = np.delete(trainXLSTM, remove, 0)
# trainY = np.delete(trainY, remove, 0)

trainCfMat = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
testCfMat = confusion_matrix(oheDecode(testY), oheDecode(testY))
cw = {i: sum(trainCfMat[j,j] for j in range(trainY.shape[1]))/trainY.shape[1]/trainCfMat[i,i] for i in range(trainY.shape[1])}
print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat)/sum(np.diag(trainCfMat)))
print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat)/sum(np.diag(testCfMat)))
print("classes weights:", cw)
print("Shape X:", trainXLSTM.shape)

bootstrap = 0
undersample = True

if bootstrap > 0:
    (_, trainXLSTM, trainY) = oversampleSet(np.zeros((1,1)), trainXLSTM, trainY, bootstrap, 0.1)
    # (trainXi80, _, trainYi80) = oversampleSet(np.zeros((1,1)), trainXLSTMi80, trainYi80, bootstrap, 0.1)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    trainCfMat = trainCfMatBS
    print("training set after bootstrap:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))

if undersample:
    (_, trainXLSTM, trainY) = undersampleSet(np.zeros((1,1)), trainXLSTM, trainY)
    # (_, trainXLSTMi80, trainYi80) = undersampleSet(np.zeros((1,1)), trainXLSTMi80, trainYi80)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    # trainCfMatBSi80 = confusion_matrix(oheDecode(trainYi80), oheDecode(trainYi80))
    print("training set us101 after undersampling:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))
    # print("training set i80 after undersampling:", np.diag(trainCfMatBSi80), "\nfreq:", np.diag(trainCfMatBSi80)/sum(np.diag(trainCfMatBSi80)))


reorder = (np.array([i for i in range(trainY.shape[0])]))
np.random.shuffle(reorder)
trainX = trainXLSTM[reorder,:,:]
trainY = trainY[reorder,:]

# reorder = (np.array([i for i in range(trainYi80.shape[0])]))
# np.random.shuffle(reorder)
# trainXLSTMi80 = trainXLSTMi80[reorder,:,:]
# trainYi80 = trainYi80[reorder,:]

# create and fit the LSTM network
if retrain:
    model = Sequential()
#     model.add(Convolution1D(100,input_shape=trainX.shape[1]))
    model.add(Convolution1D(250, 5, input_shape=(trainXLSTM.shape[1], trainXLSTM.shape[2]), use_bias=True))
    model.add(MaxPooling1D())
    model.add(Convolution1D(250, 5, use_bias=True))
    model.add(MaxPooling1D())
    model.add(Convolution1D(250, 5, use_bias=True))
    model.add(MaxPooling1D())
    model.add(Convolution1D(250, 5, use_bias=True))
    model.add(MaxPooling1D())
    # model.add(Convolution1D(50, 5, input_shape=(trainXLSTM.shape[1], trainXLSTM.shape[2]), use_bias=True))
#     model.add(LSTM(200, input_shape=(trainXLSTM.shape[1], trainXLSTM.shape[2]), return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Flatten())
    # model.add(GlobalMaxPooling1D())
    for i in range(2):
        model.add(Dense(300, activation='elu'))
        model.add(Dropout(0.2))
    model.add(Dense(trainY.shape[1], activation='softmax'))
    opt = optimizers.adam(lr=1e-4)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    print("Number of parameters:", model.count_params())
else:
    model = None

trainhist = {'us101loss': [], 'us101val_loss': [], 'us101acc': [], 'us101val_acc': [],
             'i80loss': [], 'i80val_loss': [], 'i80acc': [], 'i80val_acc': []}
# FIT
if retrain:
    #, class_weight={0:0.29,1:1.5,2:2.1,3:8.6,4:8.8}
    for i in range(200):
        print("Iter", i+1)
        hist = model.fit(trainXLSTM, trainY, validation_split=0.2, epochs=2, verbose=0, batch_size=256)#, class_weight=cw)
        model.reset_states()
        # hist = model.fit(trainXLSTMi80, trainYi80, validation_split=0.2, epochs=2, verbose=0, batch_size=1024)#, class_weight=cw)
        # model.reset_states()
        # model.save("model-rnn_iter" + str(i) + ".ker")
        trainpred = model.evaluate(trainX, trainY, verbose=0, batch_size=2048)
        testpred = model.evaluate(testXLSTM, testY, verbose=0, batch_size=2048)
        trainhist['us101loss'] += [trainpred[0]]
        trainhist['us101val_loss'] += [testpred[0]]
        trainhist['us101acc'] += [trainpred[1]]
        trainhist['us101val_acc'] += [testpred[1]]
        # trainpredi80 = model.evaluate(trainXLSTMi80, trainYi80, verbose=0, batch_size=2048)
        # testpredi80 = model.evaluate(testXLSTMi80, testYi80, verbose=0, batch_size=2048)
        # trainhist['i80loss'] += [trainpredi80[0]]
        # trainhist['i80val_loss'] += [testpredi80[0]]
        # trainhist['i80acc'] += [trainpredi80[1]]
        # trainhist['i80val_acc'] += [testpredi80[1]]
        print("train",trainpred,"test",testpred)#, "i80 train", trainpredi80, "i80 test", testpredi80)
else:
    model = load_model('model-rnn_epoch0.ker')


predTrain = (model.predict(trainXLSTM, batch_size=2048))
predTest = (model.predict(testXLSTM, batch_size=2048))
# predTraini80 = (model.predict(trainXLSTMi80, batch_size=2048))
# predTesti80 = (model.predict(testXLSTMi80, batch_size=2048))
print("train us101\n",confusion_matrix(oheDecode(trainY), oheDecode(predTrain)))
print("test us101\n",confusion_matrix(oheDecode(testY), oheDecode(predTest)))
# print("train i80\n",confusion_matrix(oheDecode(trainYi80), oheDecode(predTraini80)))
# print("test i80\n",confusion_matrix(oheDecode(testYi80), oheDecode(predTesti80)))
print(model.evaluate(testXLSTM, testY, batch_size=2048, verbose=0))

if retrain:
    plt.figure()
    plt.plot(trainhist['us101loss'])
    plt.plot(trainhist['us101val_loss'])
    # plt.plot(trainhist['i80loss'])
    # plt.plot(trainhist['i80val_loss'])
    plt.figure()
    plt.plot(trainhist['us101acc'])
    plt.plot(trainhist['us101val_acc'])
    # plt.plot(trainhist['i80acc'])
    # plt.plot(trainhist['i80val_acc'])
    plt.show()
