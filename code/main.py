from ngsimread import *
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
import pickle
from sklearn.externals import joblib

X_train, X_test, y_train, y_test, w_train, w_test = train_test_split(X, y, w, train_size=0.7, random_state=4)
retrain = False

if load and not retrain:
    print("Loading regressor")
#    with open("regressor_%s.pkl" % suffix, 'rb') as file:
#        reg = pickle.load(file)
    with open("prediction_%s.pkl" % suffix, 'rb') as file:
        y_multirf = pickle.load(file)
else:
    print("Training regressor")
    t1 = time()
    reg = MultiOutputRegressor(RandomForestRegressor(n_estimators=100, max_depth=30, random_state=0, n_jobs=-1))
            #Pipeline(steps = [#('pca', PCA(copy=True, iterated_power='auto', n_components=None, random_state=None, svd_solver='auto', tol=0.0, whiten=False)),
#                              ('reg', RandomForestClassifier(n_estimators=30, max_depth=20, random_state=0, n_jobs=-1))])
    #)
    reg.fit(X_train, y_train, w_train)
    print("Finished training in", time() - t1)
    #with open("regressor_%s.dat" % suffix, 'wb') as file:
    with open("regressor_%s.pkl" % suffix, 'wb') as file:
        pickle.dump(reg, file, protocol=pickle.HIGHEST_PROTOCOL)
    print("Predicting")
    t1 = time()
    y_multirf = reg.predict(X_test)
    print("Finished predicting in", time() - t1)
    with open("prediction_%s.pkl" % suffix, 'wb') as file:
        pickle.dump(y_multirf, file, protocol=pickle.HIGHEST_PROTOCOL)
    print("score :", reg.score(X_test, y_test))


nstepsX = int(X_test.shape[1]/21)
nsteps = int(y_test.shape[1]/2)
for v in range(0,20,1):
    plt.figure()
    s = 20
    for i in range(nstepsX):
        plt.scatter(X_test[v, 21*i]/scaleX, X_test[v, 21*i+1]/scaleY,
                    c="navy", s=s, marker="s")
    for i in range(nsteps):
        plt.scatter(y_test[v, i]/scaleX, y_test[v, nsteps+i]/scaleY,
                    c="green", s=s, marker="s", alpha=1-i/nsteps)
        plt.scatter(y_multirf[v, i]/scaleX, y_multirf[v, nsteps + i]/scaleY,
                    c="red", s=s, marker="s", alpha=1-i/nsteps)
#        plt.axis('equal')
plt.show()