import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from time import time
import pickle

def date_parser(str):
    return pd.to_datetime(str, unit='ms')

def get8Surrounding(dataT, vid):
    # ret:
    # 0     unassigned
    # 1     rear left (follower of 4)
    # 2     rear (follower of 5)
    # 3     rear right (follower of 6)
    # 4     left (closest to the left)
    # 5     self
    # 6     right (closest to the right)
    # 7     front left (leader of 4)
    # 8     front (leader of 5)
    # 9     front right (leader of 6)
    ret = [0 for i in range(10)]
    ret[5] = vid
    selfpos = np.array(dataT[['locX', 'locY']].loc[vid])
    dist = np.sqrt(np.sum(np.square(np.array(dataT[['locX', 'locY']]) - selfpos),1))
    listV = np.unique(dataT.index.get_level_values('vid'))
    distM = {listV[i]: dist[i] for i in range(len(listV))}
    for i in range(len(listV)):
        v = listV[i]
        if v != vid:
            pos = dataT[['locX', 'locY']].loc[v]
            if pos['locX'] < selfpos[0]:
                if ret[4] == 0 or distM[ret[4]] > distM[v]:
                    ret[4] = v
            if pos['locX'] > selfpos[0]:
                if ret[6] == 0 or distM[ret[6]] > distM[v]:
                    ret[6] = v
    for i in [4,5,6]:
        if ret[i] > 0:
            ret[i+3] = dataT['leader'].loc[ret[i]]
            ret[i-3] = dataT['follower'].loc[ret[i]]
    return ret


def treatLane(lane):
    # [0]: +1 if leftmost lane, -1 if rightmost lane, 0 otherwise
    # [1]: +1 if aux lane, 0 otherwise
    # [2]: +1 if on-ramp, 0 otherwise
    # [3]: +1 if off-ramp, 0 otherwisec
    return np.array([(lane == 1).astype(int) - (lane == 6), lane == 7, lane == 8, lane == 9]).T

def findInList(listWhere, listStr):
    return [i for i in range(len(listWhere)) if listWhere[i] in listStr]


names = ['vid',             # id of the vehicle
         'frame',           # id of the frame
         'totframes',       # number of frames in which the vehicle appears
         'epoch',            # global time (ms)
         'locX',            # lateral coordinate of the front center of the vehicle (ft)
         'locY',            # longitudinal coordinate of the front center of the vehicle (ft)
         'globX',           # global X (ft)
         'globY',           # global Y (ft)
         'len',             # length of the vehicle (ft)
         'wid',             # width of the vehicle (ft)
         'class',           # class: 1 motorcycle, 2 auto, 3 truck
         'vel',             # instantaneous velocity (ft/s)
         'acc',             # instantaneous acceleration (ft/sq²)
         'lane',            # lane number (file dependent)
         'leader',          # id of the lead vehicle in the same lane, 0 if error
         'follower',        # id of the following vehicle in the same lane, 0 if error
         'spacing',         # distance between front-center of vehicle and front-center of the leader (ft)
         'headway'          # time from front-center of vehicle to front-center of the leader (s), 9999.99 if error
         ]

file = "data/us101/trajectories-0750am-0805am_ok.txt"
file = "data/us101/trajectories-0805am-0820am_ok.txt"
file = "data/us101/trajectories-0820am-0835am_ok.txt"
file = "data/i80/trajectories-0400-0415_ok.txt"
file = "data/i80/trajectories-0500-0515_ok.txt"
suffix = "i80_0400"
load = False
downsampling = 2

obsTime = 5
predTime = 5
scale = 1000
sampling = 100
ft2m = 0.3048
scaleX = 10
scaleY = 0.5


X = []
y = []
w = []

if load:
    with open('Xset_%s.pkl' % suffix, 'rb') as file:
        X = pickle.load(file)
    with open('yset_%s.pkl' % suffix, 'rb') as file:
        y = pickle.load(file)
    with open('wset_%s.pkl' % suffix, 'rb') as file:
        w = pickle.load(file)
else:
    np.random.seed(0)
    data = pd.read_csv(file, header=None, names=names, sep=' ') #, parse_dates=['time'], date_parser=date_parser)
    # relative time from start of experiment
    data['epoch'] = data['epoch'] - min(data['epoch'].values)
    # scale feet to meters
    data[['locX', 'locY', 'len', 'wid', 'vel', 'acc']] *= ft2m
    # compute velocities
    # lateral velocity
    data['vx'] = data.groupby(by='vid')['locX'].diff() * (scale / sampling)
    # longitudinal velocity
    data['vy'] = data.groupby(by='vid')['locY'].diff() * (scale / sampling) * scaleY
    # indicator for lane status
    lane = treatLane(data['lane'])
    # add lane indicator to the dataset
    data = data.join(pd.DataFrame(data=lane, index=data.index, columns=['l0', 'l1', 'l2', 'l3']))
    # sets index for faster access
    data_idx = data.set_index(['vid', 'epoch'])

    # list of features to be extracted from the csv (for all 8 surrounding vehicles, plus self)
    # local X, Y, length, width, vx and vy, acceleration and class
    ft = ['locX', 'locY', 'len', 'wid', 'vx', 'vy', 'acc', 'class']
    # list of features to be extracted only for self
    # headway, id of leader, indicators for lane
    ftSelf = ['headway', 'leader', 'l0', 'l1', 'l2', 'l3']
    listft = [f + '_x' for f in ft] + ftSelf + [f + '_y' for f in ft]
    # list of targets to be extracted
    listtarg = ['locX_x', 'locY_x']
    # list of columns to drop
    drop = ['leader']

    # append info on the leading vehicle
    pdmerge = pd.merge(data[['vid', 'epoch'] + ft + ftSelf], data_idx[ft], how='left', left_on=['leader', 'epoch'], right_index=True, suffixes=('_5', '_8'))
    # remove NAs
    pdmerge = pdmerge.fillna(value=0)
    pdmerge_idx = pdmerge.set_index(['vid', 'epoch'])

    print(pdmerge.head())
    quit()

    numfeat = 0
    numtarg = 0
    datT = pdmerge_idx[listft]
    indices = np.asarray(list(datT.index.values))
    listT = indices[:,1]
    datT = datT.values

    startT = min(listT)
    endT = max(listT)
    print("start", startT, "end", endT, "dur", (endT-startT)/1000)
    print("Computing dataset")

    for t0 in range(startT, endT - (obsTime+predTime)*scale, scale):
        print((t0-startT)/1000, (t0 + (obsTime+predTime)*scale - startT)/1000) #.loc[np.logical_and(listT >= t0, listT<= t0 + (obsTime+predTime)*scale)]
        listv = np.intersect1d(indices[listT == t0,0], indices[listT == t0 + (obsTime+predTime)*scale,0])
        timerange = np.logical_and(listT >= t0, listT <= t0 + (obsTime+predTime)*scale)
        datR = datT[timerange, :]
        irange = indices[timerange,:]
        obsr = irange[:,1] < t0 + obsTime*scale
        obsrange = obsr
        predrange = np.logical_not(obsr)
        curtimeIndex = irange[:,1] == t0 + obsTime*scale
        for v in listv:
            vindex = irange[:,0] == v
            obsindex = np.logical_and(vindex,obsrange)
            predindex = np.logical_and(vindex, predrange)
            curindex = np.logical_and(vindex, curtimeIndex)
            y0 = datR[curindex,1]
            feat = datR[obsindex,:]
            targ = datR[predindex,:][:,findInList(listft, listtarg)]
            xdat = findInList(listft, ['locX_x', 'locX_y'])
            ydat = findInList(listft, ['locY_x', 'locY_y'])
            feat[:, xdat] = feat[:, xdat]*scaleX
            feat[:, ydat] = (feat[:, ydat] - y0)*scaleY
            #p.array([(feat[:, 1] - y0), (feat[:, 14] - y0*(feat[:,8] > 0).astype(int))]).T/scaleY
            feat = np.delete(feat, findInList(listft, 'leader'), axis=1)
            feat = list(feat[::downsampling].flatten())
            targ[:, xdat[0]] = targ[:, xdat[0]]*scaleX
            targ[:, ydat[0]] = (targ[:, ydat[0]]-y0)*scaleY
            ww = 1+np.std(targ[:, xdat[0]])*100 + 10*np.std(np.diff(targ[:, ydat[0]])/(scale/sampling))
            if True or (ww > 500 or np.random.uniform() > 0.99):
                w.append(ww)
                targ = list(targ[::downsampling,0]) + list(targ[::downsampling,1])
                X += feat
                y += targ
                numfeat = len(feat)
                numtarg = len(targ)

    X = np.reshape(X, (int(len(X) / numfeat), numfeat))
    y = np.reshape(y, (int(len(y) / numtarg), numtarg))

    with open('Xset_%s.pkl' % suffix, 'wb') as file:
        pickle.dump(X, file=file, protocol=pickle.HIGHEST_PROTOCOL)
    with open('yset_%s.pkl' % suffix, 'wb') as file:
        pickle.dump(y, file=file, protocol=pickle.HIGHEST_PROTOCOL)
    with open('wset_%s.pkl' % suffix, 'wb') as file:
        pickle.dump(w, file=file, protocol=pickle.HIGHEST_PROTOCOL)

print("Dataset: X", X.shape, "y", y.shape)

#print(np.unique(data.index.get_level_values('epoch')))
#print((data.loc[data.index.get_level_values('epoch') == 1118846980200]))
#print(data.xs(1118846980200, level='epoch'))
#print(data.xs(1118847002900, level = 'epoch').index)

#get8Surrounding(data.xs(1118847002900, level = 'epoch'), 22)