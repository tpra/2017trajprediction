import math
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras import metrics
from keras.layers import Activation
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
# fix random seed for reproducibility
np.random.seed(0)
import pickle
from math import ceil

def create_dataset(data, trainset, testset, look_back, look_forward, step_size, skip_seq, columns):
    trainX, trainY, testX, testY = [], [], [], []

    numft = data.shape[2]
    numftself = numft
    diff = matchList(columns, ['locY', 'locY_8', 'locY_88', 'locY_6', 'locY_4'])
    diffref = matchList(columns, ['locY'])
    for k in trainset:
        for step in range(0, data.shape[1] - look_back - look_forward, skip_seq):
            if np.max(data[k, step:(step+look_back):step_size, 0]) == 0:
                continue
            tmp = data[k, :, :]
            for i in diff:
                tmp[:, i] = tmp[:, i]  - tmp[step, diffref[0]]
            trainX.append(np.reshape(tmp[step:(step+look_back):step_size, :], (int(look_back/step_size)*numft)))
            trainY.append(np.reshape(tmp[(step+look_back):(step+look_back+look_forward):step_size, 0:numftself], (int(look_forward/step_size)*numftself)))
    for k in testset:
        for step in range(0, data.shape[1] - look_back - look_forward, skip_seq):
            if np.max(data[k, step:(step+look_back):step_size, 0]) == 0:
                continue
            tmp = data[k, :, :]
            for i in diff:
                tmp[:, i] = tmp[:, i]  - tmp[step, diffref[0]]
            testX.append(np.reshape(tmp[step:(step+look_back):step_size, :], (int(look_back/step_size)*numft)))
            testY.append(np.reshape(tmp[(step+look_back):(step+look_back+look_forward):step_size, 0:numftself], (int(look_forward/step_size)*numftself)))
    return np.array(trainX), np.array(trainY), np.array(testX), np.array(testY)

def matchList(listWhere, listStr):
    ret = []
    for i in range(len(listStr)):
        found = -1
        for j in range(len(listWhere)):
            if listWhere[j] == listStr[i]:
                found = j
                break
        ret.append(found)
    return ret

def scale(data, scales):
    for i in range(len(scales)):
        data[:, i] /= scales[i]
    return data

def unscale(data, scales):
    for i in range(len(scales)):
        data[:, i] *= scales[i]
    return data

with open("Xset_8s_us101-0750.pkl", 'rb') as file:
    (X, columns) = pickle.load(file)

keys = list(X.keys())
nkeys = len(keys)
#nkeys = 100
sel = np.random.rand(nkeys)
trainset = list(np.squeeze(np.where(sel < 0.8)))
testset = list(np.squeeze(np.where(sel >= 0.8)))

ftOthers =  ['locX', 'vx', 'vy', 'locY'] #, 'vx', 'vy', 'wid', 'len']
ftSelf =  ['locX', 'vx', 'vy', 'locY'] #, 'vx', 'vy', 'wid', 'len']
scaleOthers = [10, 1, 10, 10]
scaleSelf = [10, 1, 10, 10]
listOthers = [8,88,4,6]
features = ftSelf + [i + '_s' + str(j) for i in ftOthers for j in listOthers]
scales = scaleSelf + [i for i in scaleOthers for j in listOthers]
ftIdx = matchList(columns, features)
look_back = 50
look_forward = 50
step_size = 5
skip_seq = 10

retrain = True

# create and fit the LSTM network
if retrain:
    model = Sequential()
    model.add(Dense(400, input_dim=len(features*int(look_back/step_size)), activation='tanh'))
    model.add(Dense(400, activation='tanh'))
    model.add(Dense(400, activation='relu'))
    model.add(Dense(400, activation='tanh'))
    model.add(Dense(len(features*int(look_back/step_size)), activation='linear'))
    model.compile(loss='mse', optimizer='adadelta', metrics=['mae'])
else:
    model = None

maxdur = 0
scaler = MinMaxScaler(feature_range=(-1, 1))
for i in range(nkeys):
    maxdur = max(maxdur, X[keys[i]].shape[0])


data = np.zeros((nkeys, maxdur, len(features)))
for i in range(nkeys):
#    rows = X[keys[i]][:,17] > 0
    datX = np.squeeze(X[keys[i]][:,ftIdx])
    rows = X[keys[i]][:,17] > 0
    datX = scale(datX[rows,:], scales)
    data[i,:,:] = np.pad(datX, pad_width=((maxdur - datX.shape[0],0), (0,0)), mode='constant', constant_values=0)

#scaler.fit(np.reshape(data, (data.shape[0]*data.shape[1], data.shape[2])))
#for i in range(nkeys):
#    data[i,:,:] = scaler.transform(data[i,:,:])

trainX, trainY, testX, testY = create_dataset(data, trainset, testset, look_back, look_forward, step_size, skip_seq, columns)
print("Shape trainX", trainX.shape, "trainY", trainY.shape)
#trainX = np.reshape(trainX, (trainX.shape[0], look_back, trainX.shape[2]))
#testX = np.reshape(testX, (testX.shape[0], look_back, testX.shape[2]))

''


# FIT
if retrain:
    model.fit(trainX, trainY, epochs=100, verbose=2, batch_size=500, shuffle=True)
    model.save("model-simplenn.ker")
else:
    model = load_model('model-simplenn.ker')
#    model.fit(trainX, trainY, epochs=100, verbose=2)
#    model.save("model600.ker")




for i in range(10): #testX.shape[0]):
    x = testX[[i],:]
    yref = unscale(np.reshape(testY[i, :], (int(look_forward/step_size), len(features))), scales)
    ypred = unscale(np.reshape(model.predict(x), (int(look_forward/step_size), len(features))), scales)
    xscaled = unscale(np.reshape(x, (int(look_forward/step_size), len(features))), scales)
    plt.figure(4*i)
    plt.plot(range(0, look_back, step_size), xscaled[:,0])
    plt.plot(range(look_back, look_back+look_forward, step_size), yref[:,0])
    plt.plot(range(look_back, look_back+look_forward, step_size), ypred[:,0])
    plt.figure(4*i+1)
    plt.plot(range(0, look_back, step_size), xscaled[:,1])
    plt.plot(range(look_back, look_back+look_forward, step_size), yref[:,1])
    plt.plot(range(look_back, look_back+look_forward, step_size), ypred[:,1])
    plt.figure(4*i+2)
    plt.plot(range(0, look_back, step_size), xscaled[:,2])
    plt.plot(range(look_back, look_back+look_forward, step_size), yref[:,2])
    plt.plot(range(look_back, look_back+look_forward, step_size), ypred[:,2])
    plt.figure(4*i+3)
    plt.plot(range(0, look_back, step_size), xscaled[:,3])
    plt.plot(range(look_back, look_back+look_forward, step_size), yref[:,3])
    plt.plot(range(look_back, look_back+look_forward, step_size), ypred[:,3])

plt.show()