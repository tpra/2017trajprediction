import os
from keras.models import Sequential, load_model, model_from_json
from keras.layers import *
from keras import backend as K
from keras import optimizers
import tensorflow as tf
from tensorflow.python.client import timeline
from keras import metrics
from keras.layers import Activation
from sklearn.preprocessing import *
from sklearn.metrics import *
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
# fix random seed for reproducibility
np.random.seed(0)
import pickle
from overundersample import *
from keras.callbacks import LearningRateScheduler

def oheDecode(data):
    data2 = np.zeros(data.shape[0])
    for i in range(data.shape[0]):
        tmp = np.where((data[i,:]) == np.max(data[i,:]))
        try:
            data2[i] = tmp[0][0]
        except:
            print(data[i,:], tmp[0])
            quit()
    return data2

retrain = True


lf = '5'

with open('nnsets_us101_lf' + lf + '.pkl', 'rb') as f:
    (trainX, trainY, testX, testY, _, _, _, _,  _, _, features) = pickle.load(f)

# with open('nnsets_i80_lf' + lf + '.pkl', 'rb') as f:
#     (trainXi80, trainYi80, testXi80, testYi80, _, _, _, _,  _, _, features) = pickle.load(f)

#trainX = np.concatenate((trainX, trainXi80))
#trainY = np.concatenate((trainY, trainYi80))

print("Done loading dataset")

trainCfMat = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
testCfMat = confusion_matrix(oheDecode(testY), oheDecode(testY))
cw = {i: sum(trainCfMat[j,j] for j in range(trainY.shape[1]))/trainY.shape[1]/trainCfMat[i,i] for i in range(trainY.shape[1])}

bootstrap = 10*np.min(np.diag(trainCfMat))
undersample = True

trainCfMatBS = trainCfMat
print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat)/sum(np.diag(trainCfMat)))
print("classes weights:", cw)
print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat)/sum(np.diag(testCfMat)))

if bootstrap > 0:
    (trainX, _, trainY) = oversampleSet(trainX, np.zeros((1,1,1)), trainY, bootstrap, 0.1)
    # (trainXi80, _, trainYi80) = oversampleSet(trainXi80, np.zeros((1,1,1)), trainYi80, bootstrap, 0.1)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    trainCfMat = trainCfMatBS
    print("training set after bootstrap:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))

if undersample:
    (trainX, _, trainY) = undersampleSet(trainX, np.zeros((1,1,1)), trainY)
    # (trainXi80, _, trainYi80) = undersampleSet(trainXi80, np.zeros((1,1,1)), trainYi80)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    print("training set after undersampling:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))


reorder = (np.array([i for i in range(trainY.shape[0])]))
np.random.shuffle(reorder)
trainX = trainX[reorder,:]
trainY = trainY[reorder,:]

# reorder = (np.array([i for i in range(trainYi80.shape[0])]))
# np.random.shuffle(reorder)
# trainXi80 = trainXi80[reorder,:]
# trainYi80 = trainYi80[reorder,:]

trainCfMat = trainCfMatBS
cw = {i: sum(trainCfMat[j,j] for j in range(trainCfMat.shape[0]))/trainCfMat.shape[0]/trainCfMat[i,i] for i in range(trainCfMat.shape[0])}

print("Shape trainX", trainX.shape, "testX", testX.shape)

# create and fit the LSTM network
if retrain:
    model = Sequential()
#    model.add(Convolution1D(100,input_shape=trainX.shape[1]))
    model.add(Dense(700, input_dim=trainX.shape[1], activation='linear'))
    model.add(Dropout(0.2))
    for i in range(5):
        model.add(Dense(300, activation='elu'))
        model.add(Dropout(0.2))
#    model.add(Dense(25, activation='relu'))
#    model.add(Dense(25, activation='relu'))
    model.add(Dense(testY.shape[1], activation='softmax'))
    opt = optimizers.adam(lr=0.00001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
else:
    model = None


# FIT
trainhist = {'us101loss': [], 'us101val_loss': [], 'us101acc': [], 'us101val_acc': [],
             'i80loss': [], 'i80val_loss': [], 'i80acc': [], 'i80val_acc': []}
# FIT
if retrain:
    #, class_weight={0:0.29,1:1.5,2:2.1,3:8.6,4:8.8}
    for i in range(200):
        print("Iter", i+1)
        hist = model.fit(trainX, trainY, validation_split=0.2, epochs=1, verbose=0, batch_size=64)#, class_weight=cw)
        # hist = model.fit(trainXi80, trainYi80, validation_split=0.2, epochs=1, verbose=0, batch_size=1024)#, class_weight=cw)
        # model.save("model-rnn_iter" + str(i) + ".ker")
        # for k in ['loss', 'val_loss', 'acc', 'val_acc']:
        #     trainhist[k] += hist.history[k]
        # predTrain = (model.predict(trainXLSTM))
        # predTest = (model.predict(testXLSTM))
        # print("train\n", confusion_matrix(oheDecode(trainY), oheDecode(predTrain)))
        # print("test\n", confusion_matrix(oheDecode(testY), oheDecode(predTest)))
        trainpred = model.evaluate(trainX, trainY, verbose=0, batch_size=2048)
        testpred = model.evaluate(testX, testY, verbose=0, batch_size=2048)
        trainhist['us101loss'] += [trainpred[0]]
        trainhist['us101val_loss'] += [testpred[0]]
        trainhist['us101acc'] += [trainpred[1]]
        trainhist['us101val_acc'] += [testpred[1]]
        # trainpredi80 = model.evaluate(trainXi80, trainYi80, verbose=0, batch_size=2048)
        # testpredi80 = model.evaluate(testXi80, testYi80, verbose=0, batch_size=2048)
        # trainhist['i80loss'] += [trainpredi80[0]]
        # trainhist['i80val_loss'] += [testpredi80[0]]
        # trainhist['i80acc'] += [trainpredi80[1]]
        # trainhist['i80val_acc'] += [testpredi80[1]]
        print("train",trainpred,"test",testpred)#, "i80 train", trainpredi80, "i80 test", testpredi80)
else:
    model = load_model('model-rnn_epoch0.ker')


predTrain = (model.predict(trainX, batch_size=2048))
predTest = (model.predict(testX, batch_size=2048))
# predTraini80 = (model.predict(trainXi80, batch_size=2048))
# predTesti80 = (model.predict(testXi80, batch_size=2048))
print("train us101\n",confusion_matrix(oheDecode(trainY), oheDecode(predTrain)))
print("test us101\n",confusion_matrix(oheDecode(testY), oheDecode(predTest)))
# print("train i80\n",confusion_matrix(oheDecode(trainYi80), oheDecode(predTraini80)))
# print("test i80\n",confusion_matrix(oheDecode(testYi80), oheDecode(predTesti80)))
print(model.evaluate(testX, testY, batch_size=2048, verbose=0))

if retrain:
    plt.figure()
    plt.plot(trainhist['us101loss'])
    plt.plot(trainhist['us101val_loss'])
    # plt.plot(trainhist['i80loss'])
    # plt.plot(trainhist['i80val_loss'])
    plt.figure()
    plt.plot(trainhist['us101acc'])
    plt.plot(trainhist['us101val_acc'])
    # plt.plot(trainhist['i80acc'])
    # plt.plot(trainhist['i80val_acc'])
    plt.show()
