import numpy as np


def oversampleSet(X, XLSTM, Y, targetnum, noiselvl):
    XBS = np.zeros((0, X.shape[1]))
    YBS = np.zeros((0, Y.shape[1]))
    XLSTMBS = np.zeros((0, XLSTM.shape[1], XLSTM.shape[2]))

    # Bootstrap  set to equalize classes
    for i in range(Y.shape[1]):
        catIdx = np.where(Y[:,i] == 1)
        bsIdx = np.random.choice(catIdx[0], max(0,targetnum-catIdx[0].shape[0]))
        if (XLSTM.shape[0] < 10):
            extractLSTM = np.zeros((0,XLSTM.shape[1],XLSTM.shape[2]))
            extract = X[bsIdx, :]
        else:
            extractLSTM = XLSTM[bsIdx, :, :]
            extract = np.zeros((0,X.shape[1]))
        noise = extract*noiselvl*np.random.normal(0,noiselvl,extract.shape)
        noiseLSTM = extractLSTM*noiselvl*np.random.normal(0,noiselvl,extractLSTM.shape)
        XBS = np.concatenate((XBS, extract+noise), axis=0)
        YBS = np.concatenate((YBS, Y[bsIdx, :]), axis=0)
        XLSTMBS = np.concatenate((XLSTMBS, extractLSTM+noiseLSTM), axis=0)

    X = np.concatenate((X, XBS), axis=0)
    Y = np.concatenate((Y, YBS), axis=0)

    return X, XLSTM, Y


def undersampleSet(X, XLSTM, Y):
    numByCat = np.sum(Y,axis=0)
    minCatNum = int(np.min(numByCat))

    XBS = np.zeros((0, X.shape[1]))
    YBS = np.zeros((0, Y.shape[1]))
    XLSTMBS = np.zeros((0, XLSTM.shape[1], XLSTM.shape[2]))

    # Bootstrap  set to equalize classes
    for i in range(Y.shape[1]):
        catIdx = np.where(Y[:,i] == 1)
        np.random.shuffle(catIdx)
        bsIdx = catIdx[0][:minCatNum]
        if (XLSTM.shape[0] < 10):
            extractLSTM = np.zeros((0,XLSTM.shape[1],XLSTM.shape[2]))
            extract = X[bsIdx, :]
        else:
            extractLSTM = XLSTM[bsIdx, :, :]
            extract = np.zeros((0,X.shape[1]))
        XBS = np.concatenate((XBS, extract), axis=0)
        YBS = np.concatenate((YBS, Y[bsIdx, :]), axis=0)
        XLSTMBS = np.concatenate((XLSTMBS, extractLSTM), axis=0)

    return XBS, XLSTMBS, YBS