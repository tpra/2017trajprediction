import numpy as np
np.random.seed(0)
from sklearn.metrics import *
import pickle
import matplotlib.pyplot as plt

def create_dataset(data, trainset, testset, look_back, look_forward, step_size, skip_seq, columns):
    trainX, trainY, testX, testY, locXYtrain, locXYtest, locXYpredtrain, locXYpredtest = [], [], [], [], [], [], [], []
    trainXLSTM = [] #np.zeros((0, int(look_back/step_size), data[0].shape[1]))
    testXLSTM = [] #np.zeros((0, int(look_back/step_size), data[0].shape[1]))

    vehicles = ['8','88','6','4','1','2','3','7','9']
    ySCols = matchList(columns, ['locY_s' + i for i in vehicles])
    xSCols = matchList(columns, ['locX_s' + i for i in vehicles])
    vySCols = matchList(columns, ['vy_s' + i for i in vehicles])
    vxSCols = matchList(columns, ['vx_s' + i for i in vehicles])
    yCol = matchList(columns, ['locY'])
    xCol = matchList(columns, ['locX'])
    vxCol = matchList(columns, ['vx'])
    vyCol = matchList(columns, ['vy'])
    laneCols = matchList(columns, ['l0', 'l1', 'l2', 'l3'])
    ftS = []
    ftAdd = []
    # hot-one:
    # same speed l+0    accel l+0   brake l+0   l+1    l-1
    for k in trainset:
        X, Y, XLSTM, locXY, locXYpred, ftS, ftAdd = treatVehicle(data[k], look_back, look_forward, step_size, xSCols, ySCols, vxSCols, vySCols, laneCols, xCol[0], yCol[0], vxCol[0], vyCol[0])
        trainX += X
        trainY += Y
        trainXLSTM += XLSTM
        locXYtrain += locXY
        locXYpredtrain += locXYpred#np.append(trainXLSTM, XLSTM, axis=0)
    for k in testset:
        X, Y, XLSTM, locXY, locXYpred, ftS, ftAdd = treatVehicle(data[k], look_back, look_forward, step_size, xSCols, ySCols, vxSCols, vySCols, laneCols, xCol[0], yCol[0], vxCol[0], vyCol[0])
        testX += X
        testY += Y
        testXLSTM += XLSTM
        locXYtest += locXY
        locXYpredtest += locXYpred
    ftNew = [f + '_' + i for f in ftS for i in vehicles] + ftAdd
    return np.array(trainX), np.array(trainY), np.array(testX), np.array(testY), np.array(trainXLSTM), np.array(testXLSTM), np.array(locXYtrain), np.array(locXYtest), np.array(locXYpredtrain), np.array(locXYpredtest), ftNew

def treatVehicle(vdata, look_back, look_forward, step_size, xSCols, ySCols, vxSCols, vySCols, laneCols, xCol, yCol, vxCol, vyCol):
    X, Y = [], []
    XLSTM = []
    locXY = []
    locXYpred = []#np.zeros((0, int(look_back/step_size), vdata.shape[1]))
    skip = 0
    ftS = ['(vxS)','(vy-vyS)','(ttc)','(yS-y)','(xS-x)']
    ftAdd = ['vx','vy','l0','l1','l2','l3']
    for step in range(0, vdata.shape[0] - look_back - look_forward):
        if skip > 0:
            skip -= 1
            continue
        if np.max(vdata[step:(step+look_back):step_size, vyCol]) < 5/10.:
            continue
        ncols = len(ySCols)
        nfields = len(ftS)
        xData = np.zeros((int(look_back/step_size), len(ftAdd)+nfields*ncols))
        xData[:,nfields*ncols+0] = vdata[step:(step+look_back):step_size, vxCol]
        xData[:,nfields*ncols+1] = vdata[step:(step+look_back):step_size, vyCol]
        for i in range(len(laneCols)):
            xData[:,nfields*ncols+i+2] = vdata[step:(step+look_back):step_size, laneCols[i]]
        xyData = vdata[step:(step+look_back):step_size, [xCol, yCol]]
        xyDataPred = vdata[(step+look_back):(step+look_back+look_forward):step_size, [xCol, yCol]]
        colidx = 0
        deltaV = np.zeros((int(look_back/step_size),len(ySCols)))

        for i in range(ncols):
            xData[:,colidx] = vdata[step:(step+look_back):step_size, vxSCols[i]]    # vx
            xData[:,colidx+ncols] = vdata[step:(step+look_back):step_size, vyCol] - vdata[step:(step+look_back):step_size, vySCols[i]]      # delta vy
            xData[xData[:,colidx+ncols] == 0,colidx+ncols] = 0.0001
            xData[:,colidx+ncols] = np.sign(xData[:,colidx+ncols])*np.maximum(0.01, np.abs(xData[:,colidx+ncols]))
            xData[:,colidx+2*ncols] = -(vdata[step:(step+look_back):step_size, yCol] - vdata[step:(step+look_back):step_size, ySCols[i]])/xData[:,colidx+ncols]  # ttc
            xData[xData[:,colidx+2*ncols] > 10,colidx+2*ncols] = 10.
            xData[xData[:,colidx+2*ncols] < -10,colidx+2*ncols] = -10.
            xData[:,colidx+2*ncols] /= 10
            if (np.any(np.isnan(xData))):
                print(xData[:,colidx+ncols])
                quit()
            xData[:,colidx+3*ncols] = (vdata[step:(step+look_back):step_size, ySCols[i]] - vdata[step:(step+look_back):step_size, yCol])
            xData[:,colidx+4*ncols] = (vdata[step:(step+look_back):step_size, xSCols[i]] - vdata[step:(step+look_back):step_size, xCol])
            colidx += 1

        X.append(np.reshape(xData, (xData.shape[0]*xData.shape[1])))
        XLSTM += [xData]
        locXY += [xyData]
        locXYpred += [xyDataPred]#np.append(XLSTM, np.reshape(vdata[step:(step+look_back):step_size, :], (1, int(look_back/step_size), vdata.shape[1])), axis=0)
        datY = classifyTraj(vdata, step + look_back, step + look_back, look_forward, xCol, vyCol)
        if datY[0] == 1:
            skip = skip_seq     # avoid too many "constant speed and lane"
        Y.append(datY)
        # if datY[3] == 1:
        #     plt.plot(xyData[:,0]*10,xyData[:,1]*10)
        #     plt.plot(xyDataPred[:,0]*10,xyDataPred[:,1]*10)
        #     plt.show()
    return X, Y, XLSTM, locXY, locXYpred, ftS, ftAdd

def classifyTraj(vdata, obsStart, obsStop, predHz, locXCol, vyCol):
    numh1 = 0
    if (vdata[obsStop+predHz, locXCol] - vdata[obsStart, locXCol]) > 2 / 10:
        numh1 = 3
    elif (vdata[obsStop+predHz, locXCol] - vdata[obsStart, locXCol]) < -2 / 10:
        numh1 = 4
    else:
        if (vdata[obsStop+predHz, vyCol] - vdata[obsStart, vyCol]) > 3 / 10:
            numh1 = 1
        elif (vdata[obsStop+predHz, vyCol] - vdata[obsStart, vyCol]) < -3 / 10:
            numh1 = 2
    # numh1 = min(1,numh1)
    datY = np.zeros(5)
    datY[numh1] = 1
    return datY

def matchList(listWhere, listStr):
    ret = []
    for i in range(len(listStr)):
        found = -1
        for j in range(len(listWhere)):
            if listWhere[j] == listStr[i]:
                found = j
                break
        ret.append(found)
    return ret

def scale(data, scales):
    for i in range(len(scales)):
        data[:, i] /= scales[i]
    return data

def unscale(data, scales):
    for i in range(len(scales)):
        data[:, i] *= scales[i]
    return data

def oheDecode(data):
    data2 = np.zeros(data.shape[0])
    for i in range(data.shape[0]):
        tmp = np.where((data[i,:]) == np.max(data[i,:]))
        try:
            data2[i] = tmp[0][0]
        except:
            print(data[i,:], tmp[0])
            quit()
    return data2

# with open("Xset_8s_us101-0750.pkl", 'rb') as file:
#     (X1, columns) = pickle.load(file)
# with open("Xset_8s_us101-0805.pkl", 'rb') as file:
#     (X2, columns) = pickle.load(file)
# with open("Xset_8s_us101-0820.pkl", 'rb') as file:
#     (X3, columns) = pickle.load(file)
#
# X = {**X1, **X2, **X3}
# with open('Xset_8s_us101-extract.pkl', 'wb') as f:
#     pickle.dump((X, columns), f, protocol=pickle.HIGHEST_PROTOCOL)

with open("dataset_8s_us101-all.pkl", 'rb') as file:
    (X, columns) = pickle.load(file)

keys = list(X.keys())
nkeys = len(keys)
#nkeys = 100
sel = np.random.rand(nkeys)
pcvalid = 0.2
trainset = list(np.squeeze(np.where(sel < 1-pcvalid)))
testset = list(np.squeeze(np.where(sel >= 1-pcvalid)))

ftOthers =  ['locX', 'locY', 'vx', 'vy'] #, 'vx', 'vy', 'wid', 'len']
ftSelf =  ['locX', 'locY', 'vx', 'vy', 'l0', 'l1', 'l2', 'l3'] #, 'vx', 'vy', 'wid', 'len']
scaleOthers = [10, 10, 1, 10]
scaleSelf = [10, 10, 1, 10, 1, 1, 1, 1]
listOthers = ['8','88','6','4','1','2','3','7','9']
features = ftSelf + [i + '_s' + str(j) for i in ftOthers for j in listOthers]
scales = scaleSelf + [i for i in scaleOthers for j in listOthers]
ftIdx = matchList(columns, features)
look_back = 30
look_forward = 50
step_size = 2
skip_seq = 5


data = {}
i = 0
for k in keys:
    data[i] = scale(np.squeeze(X[k][:,ftIdx]),scales)
    del X[k]
    i += 1
del X

trainX, trainY, testX, testY, trainXLSTM, testXLSTM, locXYtrain, locXYtest, locXYpredtrain, locXYpredtest, ftNew = create_dataset(data, trainset, testset, look_back, look_forward, step_size, skip_seq, features)
print("Shape trainX", trainX.shape, "trainY", trainY.shape, "testX", testX.shape, "testY", testY.shape, "trainXLSTM", trainXLSTM.shape, "testXLSTM", testXLSTM.shape)

with open('nnsets_us101_pos_lf' + str(int(look_forward/10)) + '.pkl', 'wb') as f:
    pickle.dump((trainX, trainY, testX, testY, trainXLSTM, testXLSTM, locXYtrain, locXYtest, locXYpredtrain, locXYpredtest, ftNew), f, protocol=pickle.HIGHEST_PROTOCOL)


trainCfMat = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
testCfMat = confusion_matrix(oheDecode(testY), oheDecode(testY))
cw = {i: sum(trainCfMat[j, j] for j in range(trainY.shape[1])) / trainY.shape[1] / trainCfMat[i, i] for i in
      range(trainY.shape[1])}
print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat) / sum(np.diag(trainCfMat)))
print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat) / sum(np.diag(testCfMat)))
print("classes weights:", cw)