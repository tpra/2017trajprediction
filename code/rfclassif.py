import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import *
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
import pickle
from overundersample import *
from sklearn.externals import joblib

def oheDecode(data):
    data2 = np.zeros(data.shape[0])
    for i in range(data.shape[0]):
        tmp = np.where((data[i,:]) == np.max(data[i,:]))
        try:
            data2[i] = tmp[0][0]
        except:
            print(data[i,:], tmp[0])
            quit()
    return data2

retrain = True

lf = '5'

with open('nnsets_us101_lf' + lf + '.pkl', 'rb') as f:
    (trainX, trainY, testX, testY, _, _, _, _, _, _, features) = pickle.load(f)


# with open('nnsets_i80_lf' + lf + '.pkl', 'rb') as f:
#     (trainXi80, trainYi80, testXi80, testYi80, _, _, _, _,  _, _, features) = pickle.load(f)

#trainX = np.concatenate((trainX, trainXi80))
#trainY = np.concatenate((trainY, trainYi80))

# remove = np.where(np.logical_and(trainY[:,0] == 1, np.random.rand(trainY.shape[0]) > 0.5))
# print("Thinning class 0: removing", len(remove[0]), "elements")
# trainX = np.delete(trainX, remove, 0)
# trainY = np.delete(trainY, remove, 0)

testY = oheDecode(testY)
# testYi80 = oheDecode(testYi80)

trainCfMat = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
testCfMat = confusion_matrix(testY, testY)
cw = {i: sum(trainCfMat[j,j] for j in range(trainCfMat.shape[0]))/trainCfMat.shape[0]/trainCfMat[i,i] for i in range(trainCfMat.shape[0])}

bootstrap = 5*np.min(np.diag(trainCfMat))
undersample = True

trainCfMatBS = trainCfMat
print("training set:", np.diag(trainCfMat), "\nfreq:", np.diag(trainCfMat)/sum(np.diag(trainCfMat)))
print("test set:", np.diag(testCfMat), "\nfreq:", np.diag(testCfMat)/sum(np.diag(testCfMat)))
print("classes weights:", cw)

if bootstrap > 0:
    (trainX, _, trainY) = oversampleSet(trainX, np.zeros((1,1,1)), trainY, bootstrap, 0.1)
    # (trainXi80, _, trainYi80) = oversampleSet(trainXi80, np.zeros((1,1,1)), trainYi80, bootstrap, 0.1)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    trainCfMat = trainCfMatBS
    print("training set after bootstrap:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))

if undersample:
    (trainX, _, trainY) = undersampleSet(trainX, np.zeros((1,1,1)), trainY)
    # (trainXi80, _, trainYi80) = undersampleSet(trainXi80, np.zeros((1,1,1)), trainYi80)
    trainCfMatBS = confusion_matrix(oheDecode(trainY), oheDecode(trainY))
    print("training set after undersampling:", np.diag(trainCfMatBS), "\nfreq:", np.diag(trainCfMatBS)/sum(np.diag(trainCfMatBS)))


reorder = (np.array([i for i in range(trainY.shape[0])]))
np.random.shuffle(reorder)
trainX = trainX[reorder,:]
trainY = trainY[reorder,:]

# reorder = (np.array([i for i in range(trainYi80.shape[0])]))
# np.random.shuffle(reorder)
# trainXi80 = trainXi80[reorder,:]
# trainYi80 = trainYi80[reorder,:]

trainCfMat = trainCfMatBS
cw = {i: sum(trainCfMat[j,j] for j in range(trainCfMat.shape[0]))/trainCfMat.shape[0]/trainCfMat[i,i] for i in range(trainCfMat.shape[0])}

trainY = oheDecode(trainY)

clf = RandomForestClassifier(n_jobs=-1, n_estimators=100, random_state=0, max_depth=25)
#clf = AdaBoostClassifier(n_estimators=250, random_state=0)
clf.fit(trainX, trainY, sample_weight=np.array([cw[i] for i in trainY]))
# with open('rf_i80_lf' + lf + '.pkl', 'rb') as f:
#     clf = pickle.load(f)

with open('rf_us101_lf' + lf + '.pkl', 'wb') as f:
    pickle.dump(clf, f)

print("Score train:", clf.score(trainX, trainY), "test:", clf.score(testX,testY))#, "testi80:", clf.score(testXi80,testYi80))

predTrain = clf.predict(trainX)
predTest = clf.predict(testX)
# predTesti80 = clf.predict(testXi80)

print("Confusion matrices:")
print("Train\n",confusion_matrix(trainY, predTrain))
print("Test\n",confusion_matrix(testY, predTest))
# print("Test i80\n",confusion_matrix(testYi80, predTesti80))