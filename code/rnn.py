import math
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
# fix random seed for reproducibility
np.random.seed(0)
import pickle

def create_dataset3dfullseries(data, trainset, testset):
    trainX, trainY, testX, testY = [], [], [], []

    for k in trainset:
        trainX.append(data[k, :(data.shape[1]-1), :])
        trainY.append(data[k, data.shape[1]-1, :])
    for k in testset:
        testX.append(data[k, :(data.shape[1]-1), :])
        testY.append(data[k, data.shape[1]-1, :])
    return np.array(trainX), np.array(trainY), np.array(testX), np.array(testY)

def create_dataset3d(data, trainset, testset, look_back):
    trainX, trainY, testX, testY = [], [], [], []

    for k in trainset:
        for step in range(0, data.shape[1] - look_back - 1, int(look_back/2)):
            trainX.append(data[k, step:(step+look_back), :])
            trainY.append(data[k, step+look_back, :])
    for k in testset:
        for step in range(0, data.shape[1] - look_back - 1, int(look_back/2)):
            testX.append(data[k, step:(step+look_back), :])
            testY.append(data[k, step+look_back, :])
    return np.array(trainX), np.array(trainY), np.array(testX), np.array(testY)

def matchList(listWhere, listStr):
    ret = []
    for i in range(len(listStr)):
        found = -1
        for j in range(len(listWhere)):
            if listWhere[j] == listStr[i]:
                found = j
                break
        ret.append(found)
    return ret

with open("Xset_8s_i80_0400.pkl", 'rb') as file:
    X = pickle.load(file)

keys = list(X.keys())
nkeys = len(keys)
sel = np.random.rand(nkeys)
trainset = list(np.squeeze(np.where(sel < 0.8)))
testset = list(np.squeeze(np.where(sel >= 0.8)))

columns = ['vid', 'epoch', 'locX', 'locY', 'len', 'wid', 'vx', 'vy', 'acc',
       'class', 'headway', 's1', 's2', 's3', 's4', 's6', 's7', 's8', 's9',
       'l0', 'l1', 'l2', 'l3', 'locX_s1', 'locY_s1', 'len_s1', 'wid_s1',
       'vx_s1', 'vy_s1', 'acc_s1', 'class_s1', 'locX_s2', 'locY_s2', 'len_s2',
       'wid_s2', 'vx_s2', 'vy_s2', 'acc_s2', 'class_s2', 'locX_s3', 'locY_s3',
       'len_s3', 'wid_s3', 'vx_s3', 'vy_s3', 'acc_s3', 'class_s3', 'locX_s4',
       'locY_s4', 'len_s4', 'wid_s4', 'vx_s4', 'vy_s4', 'acc_s4', 'class_s4',
       'locX_s6', 'locY_s6', 'len_s6', 'wid_s6', 'vx_s6', 'vy_s6', 'acc_s6',
       'class_s6', 'locX_s7', 'locY_s7', 'len_s7', 'wid_s7', 'vx_s7', 'vy_s7',
       'acc_s7', 'class_s7', 'locX_s8', 'locY_s8', 'len_s8', 'wid_s8', 'vx_s8',
       'vy_s8', 'acc_s8', 'class_s8', 'locX_s9', 'locY_s9', 'len_s9', 'wid_s9',
       'vx_s9', 'vy_s9', 'acc_s9', 'class_s9', 'locX_s88', 'locY_s88',
       'len_s88', 'wid_s88', 'vx_s88', 'vy_s88', 'acc_s88', 'class_s88',
       's2_s88']

ftOthers =  ['locX', 'vx', 'vy'] #, 'vx', 'vy', 'wid', 'len']
features = ['locX', 'vx', 'vy'] +  [i + '_s' + str(j) for i in ftOthers for j in [1,2,3,4,8,9,88]]
ftIdx = matchList(columns, features)
look_back = 50

retrain = False

# create and fit the LSTM network
if retrain:
    model = Sequential()
    model.add(LSTM(128, batch_size=1, input_shape=(None,len(features)), return_sequences=True, stateful=True))
    model.add(LSTM(128, return_sequences=False, stateful=True))
    model.add(Dense(len(features)))
    model.compile(loss='mean_squared_error', optimizer='adam')
else:
    model = None

maxdur = 0
scaler = MinMaxScaler(feature_range=(0, 1))
for i in range(nkeys):
    maxdur = max(maxdur, X[keys[i]].shape[0])

data = np.zeros((nkeys, maxdur, len(features)))
for i in range(nkeys):
    datX = np.squeeze(X[keys[i]][:,ftIdx])
    data[i,:,:] = np.pad(datX, pad_width=((maxdur - datX.shape[0],0), (0,0)), mode='constant', constant_values=0)

scaler.fit(np.reshape(data, (data.shape[0]*data.shape[1], data.shape[2])))
for i in range(nkeys):
    data[i,:,:] = scaler.transform(data[i,:,:])

trainX, trainY, testX, testY = create_dataset3dfullseries(data, trainset, testset)
print(trainX.shape)
print(trainY.shape)
#trainX = np.reshape(trainX, (trainX.shape[0], look_back, trainX.shape[2]))
#testX = np.reshape(testX, (testX.shape[0], look_back, testX.shape[2]))

if retrain:
    for i in range(100):
        print("Epoch %d/100" % i)
        for j in range(trainX.shape[1]):
            model.fit(trainX[[j],:,:], trainY, epochs=1, verbose=2)
            model.reset_states()

    model.save("model.ker")
else:
    model = load_model('model.ker')
#    model.fit(trainX, trainY, epochs=100, verbose=2)
#    model.save("model600.ker")

testYRef = []
testYPred = []
for j in range(int(len(testset)/10)):
    v = testset[j]
    for step in [800]: #0, data.shape[1] - 2*look_back - 1, int(look_back/2)):
        x = data[[v], step:(step+look_back), :]
        y = scaler.inverse_transform(data[v, (step+look_back):(step+2*look_back), :])
        xin = x
        x = scaler.inverse_transform(np.squeeze(x))
        ypred = np.zeros((look_back, y.shape[1]))
        for i in range(look_back):
            pred = model.predict(xin)
            xin = np.concatenate((xin[:,1:,:], np.reshape(pred, (1,1,pred.shape[1]))), axis=1)
            ypred[i,:] = scaler.inverse_transform(pred)
        testYRef.append(y)
        testYPred.append(ypred)
        plt.figure(2*j)
        plt.plot(range(step,(step+look_back)), x[:,0])
        plt.plot(range(step+look_back,(step+2*look_back)), y[:,0])
        plt.plot(range(step+look_back,(step+2*look_back)), ypred[:,0])
        plt.figure(2*j+1)
        plt.plot(range(step,(step+look_back)), x[:,1])
        plt.plot(range(step+look_back,(step+2*look_back)), y[:,1])
        plt.plot(range(step+look_back,(step+2*look_back)), ypred[:,1])
plt.show()