import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from time import time
import pickle
from scipy.signal import savgol_filter

def get8Surrounding(dataT, vid):
    # ret:
    # 0     unassigned
    # 1     rear left (follower of 4)
    # 2     rear (follower of 5)
    # 3     rear right (follower of 6)
    # 4     left (closest to the left)
    # 5     self
    # 6     right (closest to the right)
    # 7     front left (leader of 4)
    # 8     front (leader of 5)
    # 9     front right (leader of 6)
    ret = [0 for i in range(10)]
    ret[5] = vid
    selfline = dataT[:,0] == vid
    selfpos = np.array(dataT[selfline, 4:6])

    dist = np.sqrt(np.sum(np.square(dataT[:, 4:6] - selfpos),1))
    listV = np.unique(dataT[:,0])
    distM = {listV[i]: dist[i] for i in range(len(listV))}
    for i in range(len(listV)):
        v = listV[i]
        vline = dataT[:, 0] == v
        if v != vid and abs(dataT[vline,13] - dataT[selfline,13]) == 1:
            pos = dataT[vline, 4:6]
            if pos[0,0] < selfpos[0,0]:
                if ret[4] == 0 or distM[ret[4]] > distM[v]:
                    ret[4] = v
            if pos[0,0] > selfpos[0,0]:
                if ret[6] == 0 or distM[ret[6]] > distM[v]:
                    ret[6] = v
    for i in [4,5,6]:
        if ret[i] > 0:
            linei = np.argwhere(dataT[:, 0] == ret[i])
            ret[i+3] = dataT[linei[0,0],14]
            ret[i-3] = dataT[linei[0,0],15]
    return ret

def treatLane(lane):
    # [0]: +1 if leftmost lane, -1 if rightmost lane, 0 otherwise
    # [1]: +1 if aux lane, 0 otherwise
    # [2]: +1 if on-ramp, 0 otherwise
    # [3]: +1 if off-ramp, 0 otherwisec
    return np.array([(lane == 1).astype(int) - (lane == 6), lane == 7, lane == 8, lane == 9]).T

def findInList(listWhere, listStr):
    return [i for i in range(len(listWhere)) if listWhere[i] in listStr]


names = ['vid',             # id of the vehicle
         'frame',           # id of the frame
         'totframes',       # number of frames in which the vehicle appears
         'epoch',            # global time (ms)
         'locX',            # lateral coordinate of the front center of the vehicle (ft)
         'locY',            # longitudinal coordinate of the front center of the vehicle (ft)
         'globX',           # global X (ft)
         'globY',           # global Y (ft)
         'len',             # length of the vehicle (ft)
         'wid',             # width of the vehicle (ft)
         'class',           # class: 1 motorcycle, 2 auto, 3 truck
         'vel',             # instantaneous velocity (ft/s)
         'acc',             # instantaneous acceleration (ft/sq²)
         'lane',            # lane number (file dependent)
         'leader',          # id of the lead vehicle in the same lane, 0 if error
         'follower',        # id of the following vehicle in the same lane, 0 if error
         'spacing',         # distance between front-center of vehicle and front-center of the leader (ft)
         'headway'          # time from front-center of vehicle to front-center of the leader (s), 9999.99 if error
         ]

# file = "data/us101/trajectories-0750am-0805am_ok.txt"
# file = "data/us101/trajectories-0805am-0820am_ok.txt"
# file = "data/us101/trajectories-0820am-0835am_ok.txt"
#file = "data/i80/trajectories-0400-0415_ok.txt"
# file = "data/i80/trajectories-0500-0515_ok.txt"
# file = "data/i80/trajectories-0515-0530_ok.txt"
#file = "data/us101/trajectories-0820am-0835am_ok.txt"
load = False

files = ["data/us101/trajectories-0750am-0805am_ok.txt","data/us101/trajectories-0805am-0820am_ok.txt","data/us101/trajectories-0820am-0835am_ok.txt"]

# Downsampling of the dataset (keep only 1 out of n lines)

# Observation and prediction time (s)
obsTime = 5
predTime = 5

# Do not change
scale = 1000
sampling = 100
dt = 0.1
ft2m = 0.3048

for file in files:
    np.random.seed(0)
    data = pd.read_csv(file, header=None, names=names, sep=' ') #, parse_dates=['time'], date_parser=date_parser)
    # relative time from start of experiment
    data['epoch'] = data['epoch'] - min(data['epoch'].values)
    # scale feet to meters
    data[['locX', 'locY', 'len', 'wid', 'vel', 'acc']] *= ft2m
    # compute velocities
    # lateral velocity
    data['vx'] = data.groupby(by='vid')['locX'].diff()/dt
    # longitudinal velocity
    data['vy'] = data.groupby(by='vid')['locY'].diff()/dt
    # indicator for lane status
    lane = treatLane(data['lane'])
    # add lane indicator to the dataset
    data = data.join(pd.DataFrame(data=lane, index=data.index, columns=['l0', 'l1', 'l2', 'l3']))
#    data_idx = data.set_index('vid')

    stitle = ['s_' + str(i) for i in range(1,10)]
    sdata = np.zeros((len(data), 9))
    for i in range(1,10):
        data['s'+str(i)] = np.zeros((len(data), 1))

    datanp = np.asarray(data)
    idx = np.asarray(data['epoch'])
    print(idx)
    listT = idx[:]
    for i in range(len(datanp)):
        if (i % 1000) == 0:
            print("Done %d/%d" % (i, len(datanp)))
        s8 = get8Surrounding(datanp[listT == datanp[i,3],:], datanp[i,0])
        sdata[i,:] = [int(s8[j]) for j in range(1,10)]

    for i in range(1, 10):
        data['s' + str(i)] = sdata[:,i-1]

    sdata = pd.DataFrame(columns=['vid','epoch','locX','locY','vx','vy'])
    vidx = datanp[:,0]
    for j in np.unique(vidx):
        tmp = datanp[vidx==j,:][:,[0,3,4,5]]
        tmp2 = np.concatenate( (tmp[:, [0,1]], savgol_filter(tmp[:,2:], 11, 1, deriv=0, axis=0),
                                savgol_filter(tmp[:,2:], 11, 1, deriv=1, delta=dt, axis=0)), axis=1)
        sdata = sdata.append(pd.DataFrame(np.array(tmp2),columns=['vid','epoch','locX','locY','vx','vy']))

    datamerge = pd.merge(data, sdata, how='left', on=['vid', 'epoch'], suffixes=('_old', ''))
    data = datamerge[names + ['vx', 'vy', 'l0', 'l1', 'l2', 'l3'] + ['s' + str(i) for i in [1,2,3,4,6,7,8,9]]]

    with open(file + '_8s.pkl', 'wb') as file:
        pickle.dump(data, file=file, protocol=pickle.HIGHEST_PROTOCOL)


#print(np.unique(data.index.get_level_values('epoch')))
#print((data.loc[data.index.get_level_values('epoch') == 1118846980200]))
#print(data.xs(1118846980200, level='epoch'))
#print(data.xs(1118847002900, level = 'epoch').index)

#get8Surrounding(data.xs(1118847002900, level = 'epoch'), 22)